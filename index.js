var CoordinateConverter = require("xy-coordinate-converter")

let x = 838462.317; 
let y = 1045334.660;
let originLat = 4.5962;        //Coord. Bogotá
let originLon = 74.0777555;    //Coord. Bogotá
let conversor = CoordinateConverter.xyToLatLon(x, y, originLat, originLon);

const Coordinate = require('coordinates-converter');
const coordWithSpaces = new Coordinate('5 00 16.36 N 75 32 2.35 W'); 
let latitud = coordWithSpaces.latitude; 
let longitud = coordWithSpaces.longitude; 
console.log(latitud);
console.log(longitud);
console.log(coordWithSpaces.toDd());